from status_code_app import StatusCodeApp
import pytest
import settings


app = StatusCodeApp()


def verify_status_code(expected_status_code):
    status_code = app.get_status_code(expected_status_code)
    if status_code != expected_status_code:
        if status_code:
            msg = f"App should respond with status code {expected_status_code}, but responded with {status_code}"
        else:
            f"App should respond with status code {expected_status_code}, but did not responded"
        assert False, msg


def test_create_app():
    app.create()


def test_start_app():
    app.start()


@pytest.mark.parametrize("status_code", settings.test.http_status_codes)
def test_http_status_code(status_code):
    '''
    Tests all HTTP status codes after app starts
    '''
    verify_status_code(status_code)


def test_pause_app():
    app.pause()


def test_app_not_respond_when_paused():
    assert app.is_unreachable(
    ), "App sould not respond when it's paused, but reponded with {res}"


def test_start_after_pause():
    app.start()


@pytest.mark.parametrize("status_code", settings.test.http_status_codes)
def test_http_status_codes(status_code):
    '''
    Tests all HTTP status codes after paused and started
    '''
    verify_status_code(status_code)


def test_stop_app():
    app.stop()


def test_app_not_respond_when_stopped():
    '''
    Tests all HTTP status codes after paused and started
    '''
    assert app.is_unreachable(
    ), "App sould not respond when it's stopped, but reponded with {res}"


def test_start():
    app.start()


@pytest.mark.parametrize("status_code", settings.test.http_status_codes)
def test_http_status_codes(status_code):
    verify_status_code(status_code)
