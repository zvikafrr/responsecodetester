from dockerized_app import DockerizedApp, DockerContainerMeta
from http_client import HttpClient
import settings


class StatusCodeApp(DockerizedApp):
    '''
    Represents the application under test    
    '''

    def __init__(self):
        super().__init__(container_meta=DockerContainerMeta(
            name=settings.application.name,
            image_name=settings.docker.image,
            ports=settings.docker.ports,
            user=settings.docker.user,
            password=settings.docker.password,
            registry=settings.docker.registry
        ))
        self._client = HttpClient(hostname='localhost',
                                  port=settings.application.port,
                                  secured=settings.application.use_https)

    def __str__(self):
        return settings.application.name

    def get_status_code(self, status_code):
        return self._client.get(f'{settings.application.urls.status_code}{status_code}').status_code

    def is_unreachable(self):
        return self._client.get().unreachable

    def _get_startup_timeout_in_sec(self):
        return settings.application.startup_time_in_sec

    def _is_ready(self):
        return self._client.get().successful
