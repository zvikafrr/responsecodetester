import docker
import settings
from time import sleep


class DockerContainerMeta:

    def __init__(self, name,
                 image_name,
                 ports,
                 user,
                 password,
                 registry):
        self.name = name
        self.image_name = image_name
        self.ports = ports
        self.user = user
        self.password = password
        self.registry = registry


class DockerizedApp:
    '''
    Abstract class for docker apps
    '''

    def __init__(self, container_meta: DockerContainerMeta):
        self.logger = settings.logging.getLogger(__name__)
        self.docker = docker.from_env()
        self.container_meta = container_meta

    def __del__(self):
        hasattr(self, 'container') and self.remove()

    def _get_startup_timeout_in_sec(self):
        '''
        Should be implemented by the inheriting application
        '''
        raise NotImplementedError

    def _is_ready(self):
        '''
        Should be implemented by the inheriting application
        '''
        raise NotImplementedError

    def is_ready(self):
        return self.container.status == 'running' and self._is_ready()

    def create(self):
        self.docker.login(username=self.container_meta.user,
                          password=self.container_meta.password,
                          registry=self.container_meta.registry)
        self.docker.images.pull(self.container_meta.image_name)
        self.container = self.docker.containers.create(image=self.container_meta.image_name,
                                                      name=self.container_meta.name,
                                                      detach=True,
                                                      ports=self.container_meta.ports)

    def start(self):
        if self.is_ready():
            self.logger.error(f"{self} is already running!")
            return
        self.logger.info(f"Starting {self}..")
        try:
            if self.container.status == 'paused':
                self.container.unpause()
            else:
                self.container.start()
            self.container.reload()
            timeout = self._get_startup_timeout_in_sec()
            is_ready = False
            while timeout:
                is_ready = self.is_ready()
                if is_ready:
                    break
                else:
                    sleep(1)
                    timeout -= 1
            if not is_ready:
                raise Exception
            self.logger.info(f"{self} has started")
        except:
            self.logger.error(f"{self} has failed to start")
            raise

    def pause(self):
        if self.container.status != 'running':
            self.logger.error(f"Unable to pause - {self} is not running!")
            return
        self.logger.info(f"Pausing {self}..")
        try:
            self.container.pause()
            self.container.reload()
            self.logger.info(f"{self} has paused")
        except:
            self.logger.error(f"{self} has failed to pause")

    def stop(self):
        if self.container.status == 'exited':
            self.logger.error(f"Unable to stop - {self} is not running!")
            return
        self.logger.info(f"Stopping {self}..")
        try:
            self.container.stop()
            self.container.reload()
            self.logger.info(f"{self} has stopped")
        except:
            self.logger.error(f"{self} has failed to stop")

    def remove(self):
        if self.container.status == 'running':
            try:
                self.stop()
            except:
                pass

        self.logger.info(f"Removing {self}")
        try:
            self.container.remove(force=True)
        except:
            self.logger.error(f"Failed to remove {self}")
