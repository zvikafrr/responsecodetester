import requests
from settings import logging


class HttpResponse:
    '''
    Model guarantees that the test code won't be effected once the http library changes
    '''

    def __init__(self, response: requests.Response):
        self.status_code = response.status_code if type(response) == requests.Response and response.status_code else 0
        self.successful = self.status_code in range(200, 300)
        self.unreachable = not self.status_code

    def __str__(self):
        return str(self.status_code)


class HttpClient:
    '''
    Simple HTTP client, based on requests lib
    '''

    def __init__(self, hostname, port, secured=True, timeout=3):
        self.logger = logging.getLogger(__name__)
        if (secured):
            protocol = 'https'
        else:
            protocol = 'http'
        self.base_url = f'{protocol}://{hostname}:{port}'
        self.timeout = timeout

    def get(self, url='') -> HttpResponse:
        composed_url = self.base_url + url
        self.logger.info(f"[GET] {composed_url}")
        try:
            res = requests.get(composed_url, timeout=self.timeout)
            res_model = HttpResponse(res)    
            msg = f'Server responded with status code {res_model}'
            self.logger.info(msg)    
            return res_model
            

        except BaseException as e:
            self.logger.error(str(e))
            return HttpResponse(None)

    '''
    For future implementation 
    '''

    def post(self, url, payload) -> HttpResponse:
        raise Exception("not implemented")

    def put(self, url, payload) -> HttpResponse:
        raise Exception("not implemented")

    def delete(self, url) -> HttpResponse:
        raise Exception("not implemented")
