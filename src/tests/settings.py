import logging as logging
import os
from fastapi import status


class application:
    name = 'WebApp'
    port = 8000
    startup_time_in_sec = 10
    use_https = False
    class urls:
        status_code = '/status_code/?status_code='


class docker:
    image = os.getenv('IMAGE')
    user = os.getenv('DOCKER_USER')
    password = os.getenv('DOCKER_PASS')
    registry = os.getenv('DOCKER_REGISTRY')
    ports = {f'{application.port}/tcp': application.port}


class report:
    logging.basicConfig(level=logging.INFO)


class test:
    http_status_codes = [status_code for status_code in [eval(f'status.{item}') for item in dir(
        status) if not item.startswith("__") if eval(f'status.{item}')] if status_code in range(200, 600)]
    
