#!/bin/sh
dockerd -H tcp://localhost:2375 -H unix:///var/run/docker.sock > /dev/null 2>&1 &
until docker ps > /dev/null 2>&1
do
sleep .1
done

exec "$@"