from fastapi import FastAPI, Response

app = FastAPI()


@app.get("/")
def default():
    pass


@app.get("/status_code/")
def generate_status_code(status_code: int, response: Response):
    '''
    simple api echos response code
    '''
    response.status_code = status_code
