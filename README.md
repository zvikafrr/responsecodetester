# ResponseCodeTester

Example for a rubost, maintainable and portable web server testing

## AUT Description

webapp.py is a simple application built on top of FastApi\
The app responds with a given status code based on the request URL parameter

![Swagger Snippet](pics/%E2%80%8F%E2%80%8Fstatus_code.PNG "StatusCode API")


## Test Architecture

The tests are written in Python with Pytest and running on top of DinD Docker image.\
The test container pulls the application image and test all avialable status codes using the Pytest parameterized feature.\
The test module uses an representing object StatusCodeApp which inherits from DockerizedApp and allows to test the status code and change the container state.

![Test Architecture](pics/%E2%80%8F%E2%80%8Ftest_arch.PNG "Test Architecture")

### Key Consepts

- [ ] Portability
- [ ] Verbos
- [ ] SRP
- [ ] Event-Driven
- [ ] Permutation